+++
title = "Slides with logo example"
outputs = ["Reveal"]
[logo]
src = "gitlab-logo-100.png"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.2
highlight_theme = "color-brewer"
transition = "slide"
transition_speed = "fast"
[reveal_hugo.templates.hotpink]
class = "hotpink"
background = "#FF4081"
+++


## Example Presentation

This is a basic presentation which includes a logo. All credit to https://reveal-hugo.dzello.com/

---

[See the code for this presentation](https://gitlab.com/jrreid/hugo-reveal-demo)

---

😎 Enjoy!