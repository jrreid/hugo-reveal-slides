# Example [Hugo] static site with GitLab Pages and RevealJS

[Demo Pages Site](https://jrreid.gitlab.io/hugo-reveal-slides/)

Write Markdown and create beautiful slides, hosted as a static site with GitLab Pages

Uses https://github.com/dzello/reveal-hugo

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
